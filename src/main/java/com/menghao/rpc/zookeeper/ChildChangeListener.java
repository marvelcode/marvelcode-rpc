package com.menghao.rpc.zookeeper;

/**
 * <p>节点变更监听器.</br>
 *
 * @author MarvelCode
 */
public interface ChildChangeListener {

    /**
     * ZK节点变更监听
     *
     * @param path 变更节点路径
     * @param data 对应节点数据
     */
    void onChange(String path, String data);

}
