package com.menghao.rpc.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>Tcp模式自定义属性类.</br>
 *
 * @author MarvelCode
 */
@Data
@ConfigurationProperties(prefix = "marvel.rpc.tcp")
public class RpcTcpProperties {
    /**
     * 预期底层接口
     */
    private int exceptPort = DEFAULT_EXCEPT_PORT;
    private static final int DEFAULT_EXCEPT_PORT = 8080;
    /**
     * 最大数据长度
     */
    private int maxFrameLength = DEFAULT_MAX_FRAME_LENGTH;
    private static final int DEFAULT_MAX_FRAME_LENGTH = Integer.MAX_VALUE;
    /**
     * 读间隔
     */
    private int readIdle = DEFAULT_READ_IDLE;
    private static final int DEFAULT_READ_IDLE = 2000;
    /**
     * 写间隔
     */
    private int writIdle = DEFAULT_WRIT_IDLE;
    private static final int DEFAULT_WRIT_IDLE = 2000;
    /**
     * 连接超时
     */
    private int connectTimeout = DEFAULT_CONNECT_TIMEOUT;
    private static final int DEFAULT_CONNECT_TIMEOUT = 5000;
}
