/*
 * Copyright (C), 2002-2013, 苏宁易购电子商务有限公司
 * FileName: RandomGenerator.java
 * Author:   L.J.W
 * Date:     2013-8-7 下午4:53:58
 * Description: Passport Client
 * History:
 * <author>      <time>      <version>    <desc>
 * L.J.W         20130807        1.0.0      Passport Client
 */
package com.menghao.rpc.util;

import java.util.UUID;

/**
 * <p>创建请求ID工具类.<br>
 *
 * @author MarvelCode.
 */
public class RequestIdUtils {

    private static String BASE;

    private static AtomicPositiveInteger COUNTER = new AtomicPositiveInteger();

    static {
        BASE = ProviderHostUtils.getLocalHost() + "-" + UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static String nextId() {
        return BASE + "_" + COUNTER.incrementAndGet() + "-" + System.currentTimeMillis();
    }
}
