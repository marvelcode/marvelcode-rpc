package com.menghao.rpc.serialize;

/**
 * <p>对象序列化.<br>
 *
 * @author MarvelCode.
 */
public interface ObjectOutput extends java.io.ObjectOutput {
}
