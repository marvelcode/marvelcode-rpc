package com.menghao.rpc.serialize;

import java.io.IOException;

/**
 * <p>对象反序列化.<br>
 *
 * @author MarvelCode.
 */
public interface ObjectInput extends java.io.ObjectInput {

    /**
     * 读取对象
     */
    Object readObject(Class<?> clazz) throws IOException;
}
