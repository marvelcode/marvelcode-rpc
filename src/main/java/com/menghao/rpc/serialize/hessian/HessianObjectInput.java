package com.menghao.rpc.serialize.hessian;

import com.caucho.hessian.io.Hessian2Input;
import com.menghao.rpc.serialize.ObjectInput;

import java.io.IOException;
import java.io.InputStream;

/**
 * <p>Hessian反序列化.<br>
 *
 * @author MarvelCode.
 */
public class HessianObjectInput implements ObjectInput {

    private Hessian2Input hessian2Input;

    private InputStream is;

    public HessianObjectInput(InputStream is) {
        this.is = is;
        hessian2Input = new Hessian2Input(is);
        hessian2Input.setSerializerFactory(HessianSerializer.SERIALIZER_FACTORY);
    }

    @Override
    public Object readObject(Class<?> clazz) throws IOException {
        return hessian2Input.readObject(clazz);
    }

    @Override
    public Object readObject() throws IOException {
        return hessian2Input.readObject();
    }

    @Override
    public int read() throws IOException {
        return hessian2Input.read();
    }

    @Override
    public int read(byte[] b) throws IOException {
        return this.read(b, 0, b.length);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        return hessian2Input.readBytes(b, off, len);
    }

    @Override
    public long skip(long n) throws IOException {
        return is.skip(n);
    }

    @Override
    public int available() throws IOException {
        return is.available();
    }

    @Override
    public void close() throws IOException {
        hessian2Input.close();
    }

    @Override
    public void readFully(byte[] b) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void readFully(byte[] b, int off, int len) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public int skipBytes(int n) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean readBoolean() throws IOException {
        return hessian2Input.readBoolean();
    }

    @Override
    public byte readByte() throws IOException {
        return (byte) hessian2Input.readByte();
    }

    @Override
    public int readUnsignedByte() throws IOException {
        return hessian2Input.readByte();
    }

    @Override
    public short readShort() throws IOException {
        return hessian2Input.readShort();
    }

    @Override
    public int readUnsignedShort() throws IOException {
        return hessian2Input.readShort();
    }

    @Override
    public char readChar() throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public int readInt() throws IOException {
        return hessian2Input.readInt();
    }

    @Override
    public long readLong() throws IOException {
        return hessian2Input.readLong();
    }

    @Override
    public float readFloat() throws IOException {
        return hessian2Input.readFloat();
    }

    @Override
    public double readDouble() throws IOException {
        return hessian2Input.readDouble();
    }

    @Override
    public String readLine() throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public String readUTF() throws IOException {
        return hessian2Input.readString();
    }
}
