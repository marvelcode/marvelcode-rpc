package com.menghao.rpc.serialize;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * <p>序列化接口.<br>
 *
 * @author MarvelCode.
 */
public interface Serializer {
    /**
     * 请求内容类型
     *
     * @return String
     */
    String contentType();

    /**
     * 序列化指定对象
     *
     * @param out    输出流
     * @param object 待序列化对象
     */
    void serialize(OutputStream out, Object object) throws IOException;

    /**
     * 反序列化指定输入流
     *
     * @param in 输入流
     * @return object 反序列化后对象
     */
    Object deserialize(InputStream in) throws IOException;

    /**
     * 反序列化指定输入流
     *
     * @param in    输入流
     * @param clazz 指定反序列化class对象
     * @return T 反序列化后对象
     */
    Object deserialize(InputStream in, Class<?> clazz) throws IOException;
}
