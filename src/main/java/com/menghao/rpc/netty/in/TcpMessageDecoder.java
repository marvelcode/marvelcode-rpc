package com.menghao.rpc.netty.in;

import com.menghao.rpc.serialize.ObjectInput;
import com.menghao.rpc.serialize.hessian.HessianObjectInput;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

import java.io.InputStream;

/**
 * <p>Tcp信息解码器.</br>
 *
 * @author MarvelCode
 */
public class TcpMessageDecoder extends LengthFieldBasedFrameDecoder {


    private Class<?> covertClass;

    /**
     * @param maxFrameLength 解码时，处理每个帧数据的最大长度
     *                       0 该帧数据中，存放该帧数据的长度的数据的起始位置
     *                       4 记录该帧数据长度的字段本身的长度
     *                       0 修改帧数据长度字段中定义的值，可以为负数
     *                       4 解析的时候需要跳过的字节数
     */
    public TcpMessageDecoder(int maxFrameLength, Class<?> covertClass) {
        super(maxFrameLength, 0, 4, 0, 4);
        this.covertClass = covertClass;
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
        ByteBuf byteBuf = (ByteBuf) super.decode(ctx, in);
        if (byteBuf == null) {
            return null;
        }
        try {
            InputStream inputStream = new ByteBufInputStream(byteBuf);
            ObjectInput objectInput = new HessianObjectInput(inputStream);
            return objectInput.readObject(covertClass);
        } finally {
            byteBuf.release();
        }
    }
}
