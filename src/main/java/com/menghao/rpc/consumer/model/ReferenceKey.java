package com.menghao.rpc.consumer.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p>标识远程服务的主键.</br>
 *
 * @author MarvelCode
 */
@AllArgsConstructor
public class ReferenceKey {

    @Getter
    private Class sourceInterface;
    @Getter
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReferenceKey that = (ReferenceKey) o;

        if (sourceInterface != null ? !sourceInterface.equals(that.sourceInterface) : that.sourceInterface != null)
            return false;
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = sourceInterface != null ? sourceInterface.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}