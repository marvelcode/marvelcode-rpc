package com.menghao.rpc.provider.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p>服务提供者key，用于定位服务.</br>
 *
 * @author MarvelCode
 */
@AllArgsConstructor
public class ProviderKey {

    @Getter
    private String contract;
    @Getter
    private String implCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProviderKey that = (ProviderKey) o;

        if (contract != null ? !contract.equals(that.contract) : that.contract != null) return false;
        return implCode != null ? implCode.equals(that.implCode) : that.implCode == null;
    }

    @Override
    public int hashCode() {
        int result = contract != null ? contract.hashCode() : 0;
        result = 31 * result + (implCode != null ? implCode.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return contract + ":" + implCode;
    }
}
